package com.vayana.vnet.opstools

import org.joda.time.DateTime
import org.joda.time.Duration
import java.io.File
import java.nio.file.Paths
import java.util.*

fun Array<File>.sortByLastModified() =
    Arrays.sort(this@sortByLastModified) { f1, f2 ->
      f2.lastModified().compareTo(f1.lastModified())
    }

fun File.kbSize() = (this@kbSize.length() / 1024)

fun File.listFilesExcept(name: String): Array<File> =
    if (this@listFilesExcept.isDirectory)
      this@listFilesExcept.listFiles { f -> !(f.name.contains(name)) }
    else arrayOf()

fun DateTime.toIST(): DateTime = this@toIST.plus(Duration((5.5*60L*60L*1000L).toLong()))

fun main(args: Array<String>) {
  val dir = args[0]
  val since = DateTime().minus(Duration(args[1].toLong() * 60L * 60L * 1000L))
  val fls = Paths.get(dir).toFile()
      .listFilesExcept("empty")
      .filter { since.isBefore(it.lastModified()) }
      .toTypedArray().apply { sortByLastModified() }
  println("Total Files : ${fls.size}")
  println()
  fls.forEach {
    println("%s >> %s >> %4d kb".format(it.name, DateTime(it.lastModified()).toIST().toString("yyyy-MM-dd_HH:mm:ss"), it.kbSize()))
  }
}
